import React from "react";
// import {Loader} from "react-overlay-loader";
import {Route, Switch,BrowserRouter} from "react-router-dom";

 import Home from "../components/pages/Home";
 import Service from "../components/pages/service";
 import Contact from "../components/pages/Contact"
 import View from "../components/students/View"

 import Edit from "../components/students/Edit"


const Router = ()=>(

    <BrowserRouter>
       <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/view/:id" component={View} />

        <Route exact path="/edit/:id" component={Edit} />

        <Route exact path="/service" component={Service} />

        <Route exact path="/contact" component={Contact} />
        </Switch>
    </BrowserRouter>
);
export default Router;