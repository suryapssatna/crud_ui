import React, { useState } from "react";
import ResponsiveDialog from "../components/common/popup/ReponsivePopup.js";
import EzyDialogTitle from "../components/common/popup/EzyDialogTitle.js";
import ReponsiveContent from "../components/common/popup/ReponsiveContent.js";
import { Box, Button, Grid, Link, makeStyles, TextField, Typography } from "@material-ui/core";
import InputAdornment from "@material-ui/core/InputAdornment";
import LockIcon from '@material-ui/icons/Lock';
import EmailIcon from "@material-ui/icons/Email";
import FacebookIcon from "@material-ui/icons/Facebook";

const useStyle = makeStyles({
    extendedIcon: {
        marginRight: "12px"
    },
    footerBox: {
        color: "#fff",
        background: "#3f51b5",
    }, JoinTxt: {
        color: "#fff",
        textDecoration: "underLine",
        paddingLeft: "4px",
        fontSize: "1.1em"
    }
})

const Login = (props) => {
    const classes = useStyle();

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [page, setPage] = useState(props.LoginPop);
    
    const handleEmail = (e) => {
        setEmail(e.target.value)
    }
    const handlePassword = (e) => {
        setPassword(e.target.value)
    }
    const handleLogin = () => {
        console.log("----------email---", email);
        console.log("----------password---", password);
    }
    const handleSignUp = () => {
        console.log("----------email---", email);
        console.log("----------password---", password);
    }

    const emailTxt = () => {
        return <TextField onChange={handleEmail} autoComplete="email" name="email" variant="outlined" required fullWidth id="email" label="Email" autoFocus InputProps={{
            startAdornment: (
                <InputAdornment position="start">
                    <EmailIcon color="primary" />
                </InputAdornment>
            )
        }}>
        </TextField>
    }

    const passwordTxt = () => {
        return <TextField onChange={handlePassword} autoComplete="Password " name="Password " variant="outlined" required fullWidth id="Password " label="Password" autoFocus
            InputProps={{
                startAdornment: (
                    <InputAdornment position="start">
                        <LockIcon color="primary" />
                    </InputAdornment>
                )
            }}
            type="password">
        </TextField>
    }

    return (
        <> <ResponsiveDialog maxWidth={"lg"} open={props.open} onClose={props.onClose} >
            <EzyDialogTitle title="Login" size="small" closeAction={props.onClose} />
            <ReponsiveContent height={510}>
                <Box m={3} size="medium" align="center">
                    <Button type="submit" variant="contained" color="primary">
                        <FacebookIcon className={classes.extendedIcon} /> Log in with facebook
                    </Button>
                </Box>
                <Grid container spacing={2}>
                    <Grid item sm={12}>
                        {emailTxt()}
                    </Grid>
                    <Grid item sm={12}>
                        {passwordTxt()}
                    </Grid>
                </Grid>
                <Box m={3} size="small" align="center">
                    <Button type="submit" variant="contained" color="secondary" onClick={handleLogin}>
                        Login
                    </Button>
                </Box>
            </ReponsiveContent>
            <Box p={2} fullWidth className={classes.footerBox}>
                <Typography variant="body1">
                    Don't have account?<Link className={classes.JoinTxt} onClick={handleSignUp}>Join Now</Link>
                </Typography>
            </Box>
        </ResponsiveDialog>

        </>
    )
}
export default Login;