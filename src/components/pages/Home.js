import React, { useState } from 'react'
import { Grid, Typography, Box, makeStyles, TextField, Button,IconButton } from "@material-ui/core";
import { deepPurple, green } from '@material-ui/core/colors';
import List from "../students/List";
import axios from "axios";
import FingerprintIcon from '@material-ui/icons/Fingerprint';
import Login from '../Login';
import {LoginPop} from '../../components/config/configuration'


const useStyle = makeStyles({
  headingColor: {
    backgroundColor: deepPurple[400],
    color: "white",
  },
  addStuColor: {
    backgroundColor: green[400],
    color: "white",
  },
  loginBtn:{
    background:"#fff",
    boxShadow:'none',
    
  },
  

})

const Home = (props) => {
  const classes = useStyle();
  const [open, setOpen] = useState(false)
  
  const [student, setStudent] = useState({
    stuName: "",
    email: ""
  });
  const [status, setStatus] = useState();
  function onTextFieldChange(e) {
    console.log(e.target.value)
    setStudent({
      ...student,
      [e.target.name]: e.target.value
    })
  }

  async function onFormSubmit(e) {
    e.preventDefault()
    try {
      const stud = await axios.post(`http://localhost:5000/student`, student);
      setStatus(true)
    } catch (error) {
      console.log("Somthing is wrong");
    }
  }
  if (status) {
    return <Home />
  }
  const handelLogin =()=>{
    setOpen(true);
  }
  const onClose =()=>{
    setOpen(false)
  }
  
  return (
    <>
      <Grid container spacing={2}>
        <Grid item md={12} sm={12}>
          <Box textAlign="center" className={classes.headingColor} p={2} mb={2}>
            <Grid container spacing={2}>
              <Grid item md={10} sm={10}> <Typography variant="h2">
                React CRUD With API Call
              </Typography></Grid>
              <Grid item md={2} sm={2}> <IconButton aria-label="join" color="primary"className={classes.loginBtn} onClick={handelLogin}><FingerprintIcon className={classes.headerIcon} /></IconButton>
              </Grid>
            </Grid>
          </Box>
        </Grid>
        <Grid item md={6} xs={12}>
          <Box textAlign="center" className={classes.addStuColor} p={2} mb={2}>
            <Typography variant="h4">Add Student</Typography>
          </Box>
          <form noValidate>
            <Grid container spacing={2}>
              <Grid item sm={12}>
                <TextField onChange={e => onTextFieldChange(e)} autoComplete="stuName" name="stuName" variant="outlined" required fullWidth id="stuName" label="Name" autoFocus>
                </TextField>
              </Grid>
              <Grid item sm={12}>
                <TextField onChange={e => onTextFieldChange(e)} autoComplete="email" name="email" variant="outlined" required fullWidth id="email" label="Email Address" autoFocus>
                </TextField>
              </Grid>
            </Grid>
            <Box m={3}>
              <Button type="submit" variant="contained" color="primary" fullWidth onClick={e => onFormSubmit(e)}>
                Add
              </Button>
            </Box>
          </form>
        </Grid>
        <Grid item md={6} xs={12}>
          <List />
        </Grid>
      </Grid>
      {(open) ? <Login open={open}
      onClose={onClose} page={LoginPop} /> : ""}
    </>
  );

}

export default Home;