import React, { useEffect, useState } from 'react'
import { Grid, Typography, Box, makeStyles, TextField, Button } from "@material-ui/core";
import { deepPurple, green } from '@material-ui/core/colors'
import { useParams, useHistory } from 'react-router-dom/cjs/react-router-dom.min';
import axios from "axios";

const useStyle = makeStyles({
  headingColor: {
    backgroundColor: deepPurple[400],
    color: "white",
  },
  addStuColor: {
    backgroundColor: green[400],
    color: "white",
  },

})

const Edit = () => {
  const classes = useStyle();
  const { id } = useParams();
  const [student, setStudent] = useState({
    stuName: "",
    email: ""
  });
  const history = useHistory();

  useEffect(() => {
    getStudent();
  }, [id])

  async function getStudent() {
    try {
      const stud = await axios.get(`http://localhost:5000/student/${id}`);
      setStudent(stud.data);
    } catch (error) {
      console.log("Somthing is wrong");
    }
  }
  function handelClick() {
    history.push("/")
  }
  async function onFormSubmit(e) {
    e.preventDefault()
    try {
      const stud = await axios.put(`http://localhost:5000/student/${id}`, student);
      history.push("/")
    } catch (error) {
      console.log("Somthing is wrong");
    }
  }
  const [status, setStatus] =useState();
  function onTextFieldChange(e) {
  console.log(e.target.value)
    setStudent({
      ...student,
      [e.target.name]: e.target.value
    })
  }
  return (
    <>

      <Box textAlign="center" className={classes.headingColor} p={2} mb={2}>
        <Typography variant="h2">
          React CRUD With API Call
        </Typography>
      </Box>
      <Grid container spacing={4} justify="center">
        <Grid item xs={12} sm={6}>
          <Box textAlign="center" className={classes.addStuColor} p={2} mb={2}>
            <Typography variant="h4">Edit Student</Typography>
          </Box>
          <form >
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField autoComplete="id" name="id" variant="outlined" required fullWidth id="id" label="ID" value={id} autoFocus disabled>
                </TextField>
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField onChange={e => onTextFieldChange(e)} autoComplete="stuName" name="stuName" variant="outlined" required fullWidth id="stuName" label="Name" autoFocus value={student.stuName} >
                </TextField>
              </Grid>
              <Grid item xs={12} >
                <TextField onChange={e => onTextFieldChange(e)} autoComplete="email" name="email" variant="outlined" required fullWidth id="email" label="Email Address" autoFocus value={student.email}>
                </TextField>
              </Grid>
            </Grid>
            <Box m={3}>
              <Button type="submit" variant="contained" color="primary" fullWidth onClick={e => onFormSubmit(e)}>
                Update
              </Button>
            </Box>
          </form>
          <Box m={3} textAlign="center">
            <Button variant="contained" color="primary" onClick={handelClick}>
              Back To Home
            </Button>

          </Box>
        </Grid>


      </Grid>
    </>
  )
}

export default Edit