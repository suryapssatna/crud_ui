import React from "react";
import {  Typography, Box, makeStyles, TableContainer, Table, TableBody, TableCell, TableHead, TableRow, Paper, IconButton, Tooltip} from "@material-ui/core";
import VisibilityIcon from '@material-ui/icons/Visibility';
import { orange } from '@material-ui/core/colors';

import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import { Link } from "react-router-dom";
import axios from "axios";
import { useState, useEffect } from "react";

const useStyle = makeStyles({
    stuListColor: {
    backgroundColor: orange[400],
    color: "white",
  },
  tableHeadCell: {
    color: "white"
  }
})
const List = () => {
    const classes = useStyle();
    const[students, setStudents] =useState([]);

useEffect(()=>{
    getAllStudent();
},[])

async function getAllStudent(){
        try{
            const student = await axios.get("http://localhost:5000/student");
            setStudents(student.data);
        }catch(error){
            console.log("Somthing is wrong");
        }
    }   
    const handleDelete =async id =>{
      await axios.delete(`http://localhost:5000/student/${id}`)
      var newStudents = students.filter((item)=>{
        return item.id !== id;
      })
      setStudents(newStudents);
    }
  return (
    <>
      <Box textAlign="center" className={classes.stuListColor} p={2} mb={2}>
            <Typography variant="h4">Student List</Typography>
          </Box>
          <TableContainer component={Paper}>
            <Table>
              <TableHead>
                <TableRow style={{backgroundColor :"#616161"}}>
                    <TableCell align="center" className={classes.tableHeadCell}>No</TableCell>
                    <TableCell align="center" className={classes.tableHeadCell}>Name</TableCell>
                    <TableCell align="center" className={classes.tableHeadCell}>Email</TableCell>
                    <TableCell align="center" className={classes.tableHeadCell}>Action</TableCell>

                </TableRow>
              </TableHead>
              <TableBody>

              {students.map((student, i)=>{
                  return(<TableRow key={i}>
                <TableCell align="center">{i +1}</TableCell>
                <TableCell align="center">{student.stuName}</TableCell>
                <TableCell align="center">{student.email}</TableCell>
                <TableCell align="center">
                  <Tooltip title="View">
                  <IconButton><Link to={`/view/${student.id}`}><VisibilityIcon color="primary" /></Link></IconButton>
                  </Tooltip>
                  <Tooltip title="Edit">
                  <IconButton><Link to={`/edit/${student.id}`}><EditIcon color="primary" /></Link></IconButton>
                  </Tooltip>
                  <Tooltip title="delete">
                  <IconButton onClick={()=> handleDelete(student.id)} ><DeleteIcon color="secondary" /></IconButton>
                  </Tooltip>
                </TableCell>
                </TableRow>)
              })}
              
              </TableBody>
            </Table>
          </TableContainer>

    </>
  ) 
}

export default List;