import React from "react";
import {  Typography, Box, makeStyles, TableContainer, Table, TableBody, TableCell, TableHead, TableRow, Paper, Button} from "@material-ui/core";
import { orange } from '@material-ui/core/colors';
import {useParams, useHistory} from 'react-router-dom';
import axios from "axios";
import { useState, useEffect } from "react";

const useStyle = makeStyles({
    stuListColor: {
    backgroundColor: orange[400],
    color: "white",
  },
  tableHeadCell: {
    color: "white",
    fontWeight:"bold",
    fontSize:16
  }
})
const View = () => {
    const classes = useStyle();
    const {id} = useParams();
  const[student, setStudent] = useState([]);
    console.log("---------useParams id-----", id);
    const history = useHistory();

    useEffect(()=>{
    getStudent();
},[id])

async function getStudent(){
        try{
            const stud = await axios.get(`http://localhost:5000/student/${id}`);
            //  console.log("Somthing is Data",student);
            setStudent(stud.data);
        }catch(error){
            console.log("Somthing is wrong");
        }
    }  
    function handelClick(){
      history.push("/")
    } 
  return (
    <>
<Box textAlign="center" className={classes.stuListColor} p={2} mb={2}>
            <Typography variant="h4">Student List</Typography>
          </Box>
          <TableContainer component={Paper}>
            <Table>
              <TableHead>
                <TableRow style={{backgroundColor :"#616161"}}>
                    <TableCell align="center" className={classes.tableHeadCell}>Id</TableCell>
                    <TableCell align="center" className={classes.tableHeadCell}>Name</TableCell>
                    <TableCell align="center" className={classes.tableHeadCell}>Email</TableCell>

                </TableRow>
              </TableHead>
              <TableBody>
              <TableRow>
                <TableCell align="center">{student.id}</TableCell>
                <TableCell align="center">{student.stuName}</TableCell>
                <TableCell align="center">{student.email}</TableCell>
                
</TableRow>
              </TableBody>
            </Table>
          </TableContainer>
          <Box m={3} textAlign="center">
          <Button variant="contained" color="primary" onClick={handelClick}>
 Back To Home
          </Button>

          </Box>

    </>
  )
}

export default View