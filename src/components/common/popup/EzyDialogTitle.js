import React from 'react';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import {Grid, Typography }from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import {withStyles} from '@material-ui/core/styles';
const useStyles = theme => ({
  closeButton: {
	  margin: 0,
	  padding: 0,
	  color:"#c7c7c7"
  },
   title: {
	   color: "#fff",
	   background: "#0066a3",
   },
   titleText: {
	   color: "#fff",
	   fontSize:"2rem"
   },
   small: {
	color: "#fff",
	fontSize:"1.72rem"
},
});

function EzyDialogTitle(props) {
  const {classes} = props
  const titleText = classes[props.size || "titleText"]
  return <DialogTitle disableTypography className={classes.title}>
	<Grid container spacing={0} xs={12} direction="row" justify="flex-end" alignItems="flex-start">
		<Grid item xs>
			<Typography gutterBottom variant="h3" className={titleText}>
				{props.title}
			</Typography>
		</Grid>
		<Grid item xs={1}>
			<IconButton className={classes.closeButton} size="small" id="closeDialog" onClick={props.closeAction}>
			<CloseIcon  fontSize="small"/></IconButton>
		</Grid>
	</Grid>
</DialogTitle>
}

export default withStyles(useStyles)(EzyDialogTitle);