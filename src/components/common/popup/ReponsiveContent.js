import React from 'react';
import DialogContent from '@material-ui/core/DialogContent';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';

export default function ReponsiveContent(props) {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('sm'));

  return <DialogContent className={props.contentClass} style={isMobile?{height: "100%"}:{height: props.height, width: props.width}}>
		{props.children}
	  </DialogContent>
}
 
